#Program requirements:
def get_requirements():

    print("Painting Estimator")
    print("\nProgram Requirements:\n"
        + "1. Calculate home interior paint cost (w/o printer).\n"
        + "2. Must use float data types.\n"
        + "3. Must use SQFT_PER_GALLON constant (350).\n"
        + "4. Must use iteration structure (aka loop).\n"
        + "5. Format, right-align numbers and round to two decimal places.\n"
        + "6. Create at least five functions that are called by the progrma:\n"
                + "a. main(): calls two other functions: get_requirements() and estimate_painting_cost().\n"
                + "b. get_requirements(): displays the program requirements.\n"
                + "c. estimate_painting_cost(): calculates interior home painting, and calls print functions.\n"
                + "d. print_painting_estimate(): displays painting costs. \n"
                + "e. print_painting_percentage(): displays painting costs percentages. \n")


def estimate_painting_cost():
    
    SQFT_PER_GALLON = 350
    total_sqft = 0.0

    print("\nInput:")
    total_sqft = float(input("Enter total interior sq ft: " ))
    ppg = float(input("Enter price per gallon paint: " ))
    pr = float(input("Enter hourly painting rate per sq ft: " ))

#calculations
    paint_gallons = total_sqft / SQFT_PER_GALLON
    paint_cost = paint_gallons * ppg
    labor_cost = total_sqft * pr
    total_cost = paint_cost + labor_cost
    paint_percent = paint_cost / total_cost
    labor_percent = labor_cost / total_cost

#display painting estimates
    print_painting_estimate(total_sqft, SQFT_PER_GALLON,
                        paint_gallons, ppg, pr)

#display painting percentage
    print_painting_percentage(
        paint_cost, paint_percent, labor_cost, labor_percent, total_cost)


def print_painting_estimate(total_sqft, SQFT_PER_GALLON, paint_gallons, ppg, pr):
    print("\nOutput")
    print("{0:20} {1:>9}".format("Item", "Amount"))
    print("{0:20} {1:9,.2f}".format("Total Sq Ft: ", total_sqft))
    print("{0:20} {1:9,.2f}".format("Sq Ft per Gallon: ", SQFT_PER_GALLON))
    print("{0:20} {1:9,.2f}".format("Number of Gallons: ", paint_gallons))
    print("{0:20} ${1:8,.2f}".format("Paint per Gallon: ", ppg))
    print("{0:20} ${1:8,.2f}".format("Labor per Sq Ft: ", pr))

def print_painting_percentage(paint_cost, paint_percent, labor_cost, labor_percent, total_cost):
    print("{0:8} {1:>9} {2:>13}".format("Cost", "Amount", "Percent"))
    print("{0:8} ${1:>8,.2f} {2:13.2%}".format(
        "Paint: ", paint_cost, paint_percent))
    print("{0:8} ${1:>8,.2f} {2:13.2%}".format(
        "Labor: ", labor_cost, labor_percent))
    print("{0:8} ${1:>8,.2f} {2:13.2%}".format(
        "Total: ", total_cost, paint_percent + labor_percent))