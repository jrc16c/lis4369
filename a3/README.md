# LIS 4369

## Joshua Chanin

### Assignment 3 Requirements:


1. Write requirements
2. Intitialize variables
3. Create user input
4. Code Calculations
5. Generate Outputs
6. Code loop in main.py
7. Run in IDLE
8. Push to repository

![A3-code1](img/code1.png)
Part one of code

![A3-code2](img/code2.png)
Part two of code

![A3-idle1](img/idle1.png)
Part 1 of IDLE

![A3-idle2](img/idle2.png)
Part 2 of IDLE