import fuctions as f


def main():
    f.get_requirements()
    check = "y"
    while check.lower() == "y":
        f.estimate_painting_cost()
        print()
        check = input("Estimate another paint job? (y/n):")
        print()

    print("Thank you for using our Paint Estimator")
    print("Please visit our website: www.linkedin.com/in/joshua-chanin-240998178")
    #f.user_input()
    f.estimate_painting_cost()


if __name__ == "__main__":
    main()