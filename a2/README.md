# LIS 4369

## Joshua Chanin

### Assignment 2 Requirements:


1. Create calculate_payroll
2. Use if/else
3. Run through main.py
4. Provide screenshots of finished output
5. Create README files
6. Push to BitBucket

![A2](img/a2p1.png)
Assignment Part 1

![A2](img/a2p2.png)
Assignment Part 2



