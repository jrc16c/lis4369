#Program requirements:
def get_requirements():

    print("Payroll Calculator")
    print("\nProgram Requirements:\n"
        + "1. Must use float data type for user input.\n"
        + "2. Overtime rate: 1.5 times hourly rate (hours over 40).\n"
        + "3. Holiday rate: 2.0 times hourly rate (all holiday hours).\n"
        + "4. Must format currency with dollar sign, and round to two decimal places.\n"
        + "5. Create at least three functions that are called by the program:\n"
                + "a. main(): calls at least two other functions.\n"
                + "b. get_requirements(): displays the program requirements.\n"
                + "c. calculate_payroll(): calculates an individual one-week paycheck.\n")


def calculate_payroll():
    base_hours = 0.0
    ot_rate = 0.0
    holiday_rate = 0.0

    print("\nInput:")
    hours = float(input("Enter hours worked: " ))
    holiday_hours = float(input("Enter holiday hours: " ))
    pay_rate = float(input("Enter hourly pay rate: " ))


#calculations
    base_pay = 40 * pay_rate
    
    if hours <= 40:
        overtime_hours = 0
    elif hours > 40:
        overtime_hours = (hours - 40) * 1.5 * pay_rate

    if hours <= 40:
        holiday_pay = holiday_hours * 2.0 * pay_rate
    elif hours > 40:
        holiday_pay = (hours - 40) * 2.0 * pay_rate
    
    
    
    gross_pay = base_pay + overtime_hours + holiday_pay



#display results
    print("\nOutput:")
    print("{0:10} ${1:,.2f}".format("Base:", base_pay))
    print("{0:10} ${1:,.2f}".format("Overtime:", overtime_hours))
    print("{0:10} ${1:,.2f}".format("Holiday:", holiday_pay))
    print("{0:10} ${1:,.2f}".format("Base:", gross_pay))
    
    
    
