# LIS 4369

## Joshua Chanin

### Assignment 5 Requirements:


1. Install RStudio
2. Learn the difference between R and Python
3. Run R code
4. Push screenshots of graphs to repository

![A5-graph1](img/a5p1.png)

![A5-graph2](img/a5p2.png)

![A5-graph3](img/a5p3.png)