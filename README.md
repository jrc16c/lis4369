# LIS 4369
## Joshua Chanin

### Class Number Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install Python
    - Install R
    - Install R Studio
    - Install Visual Studio Code
    - Create a1_tip_calculator application
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorial (bitbucktstationlocations)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create calculate_payroll
    - Use if/else
    - Run through main.py
    - Provide screenshots of finished output
    - Create README files
    - Push to BitBucket

    
3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create estimate_painting_cost
    - Code a loop in main.py
    - Run in IDLE
    - Push to BitBucket

4. [P1 README.md](p1/README.md "My P1 README.md file")
    - Follow tutorials to import pandas
    - Print lines of from imported code
    - Push screenshots to repository

5. [A4 README.md](a4/README.md "My A4 README.md file")
    - Import numpy
    - Import pandas
    - Import matplotlib
    - run functions.py
    - Push screenshots to repository

6. [A5 README.md](a5/README.md "My A5 README.md file")
    - Install RStudio
    - Learn the difference between R and Python
    - Run R code
    - Push screenshots of graphs to repository

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Use code from assignment 5
    - Import new url
    - Run R code
    - Push screenshots of graphs to repository