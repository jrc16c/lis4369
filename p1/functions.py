#Program requirements:
def get_requirements():

    print("Data Analysis")
    print("\nProgram Requirements:"
        + "1. Run demo.py.\n"
        + "2. If errors, more than likely missing installations.\n"
        + "3. Test Python Package installer: pip freeze.\n"
        + "4. Research how to do the following installations.\n"
                + "a. pandas (only if missing).\n"
                + "b. pandas-datareader (only if missing).\n"
                + "c. matplotlib (only if missing).\n"
        +"5. Create at least three functions that are called by the program:\n"
                + "a. main(): calls at least two other functions.\n"
                + "b. get_requirements(): displays the program requirements.\n"
                + "c. data_analysis_1(): displays the following data.\n")

    import pandas as pd
    import datetime
    import pandas_datareader as pdr
    import matplotlib.pyplot as plt
    from matplotlib import style

    start = datetime.datetime(2010, 1, 1)
    end = datetime.datetime(2018, 10, 15)

    df = pdr.DataReader("XOM", "yahoo", start, end)

    print("\nPrint number of records: ")
   # need number of records

    print("\nPrint columns: ")
    print(df.columns)

    print("\nPrint data frame: ")
    print(df)

    print("\nPrint first five lines: ")
    #need first five lines
    print(df.iloc[:5])

    print("\nPrint first 2 lines: ")
    #need first two lines
    print(df.iloc[:2])

    print("\nPrint last 2 lines: ")
    #need last two lines
    print(df.iloc[2:])

    style.use('ggplot')

    df['High'].plot()
    df['Adj Close'].plot()
    plt.legend()
    plt.show()