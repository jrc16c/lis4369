#Program requirements:
def get_requirements():

    print("Data Analysis")
    print("\nProgram Requirements:\n"
        + "1. Run demo.py.\n"
        + "2. If errors, more than likely missing installations.\n"
        + "3. Test Python Package installer: pip freeze.\n"
        + "4. Research how to do the following installations.\n"
                + "a. pandas (only if missing).\n"
                + "b. pandas-datareader (only if missing).\n"
                + "c. matplotlib (only if missing).\n"
        +"5. Create at least three functions that are called by the program:\n"
                + "a. main(): calls at least two other functions.\n"
                + "b. get_requirements(): displays the program requirements.\n"
                + "c. data_analysis_1(): displays the following data.\n")

def estimate_painting_cost():
    
    SQFT_PER_GALLON = 350
    total_sqft = 0.0

    print("\nInput:")
    total_sqft = float(input("Enter total interior sq ft: " ))
    ppg = float(input("Enter price per gallon paint: " ))
    pr = float(input("Enter hourly painting rate per sq ft: " ))

#calculations
    paint_gallons = total_sqft / SQFT_PER_GALLON
    paint_cost = paint_gallons * ppg
    labor_cost = total_sqft * pr
    total_cost = paint_cost + labor_cost
    paint_percent = paint_cost / total_cost
    labor_percent = labor_cost / total_cost

#display painting estimates
    print_painting_estimate(total_sqft, SQFT_PER_GALLON,
                        paint_gallons, ppg, pr)

#display painting percentage
    print_painting_percentage(
        paint_cost, paint_percent, labor_cost, labor_percent, total_cost)


def print_painting_estimate(total_sqft, SQFT_PER_GALLON, paint_gallons, ppg, pr):
    print("\nOutput")
    print("{0:20} {1:>9}".format("Item", "Amount"))
    print("{0:20} {1:9,.2f}".format("Total Sq Ft: ", total_sqft))
    print("{0:20} {1:9,.2f}".format("Sq Ft per Gallon: ", SQFT_PER_GALLON))
    print("{0:20} {1:9,.2f}".format("Number of Gallons: ", paint_gallons))
    print("{0:20} ${1:8,.2f}".format("Paint per Gallon: ", ppg))
    print("{0:20} ${1:8,.2f}".format("Labor per Sq Ft: ", pr))

def print_painting_percentage(paint_cost, paint_percent, labor_cost, labor_percent, total_cost):
    print("{0:8} {1:>9} {2:>13}".format("Cost", "Amount", "Percent"))
    print("{0:8} ${1:>8,.2f} {2:13.2%}".format(
        "Paint: ", paint_cost, paint_percent))
    print("{0:8} ${1:>8,.2f} {2:13.2%}".format(
        "Labor: ", labor_cost, labor_percent))
    print("{0:8} ${1:>8,.2f} {2:13.2%}".format(
        "Total: ", total_cost, paint_percent + labor_percent))