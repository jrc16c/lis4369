# LIS 4369

## Joshua Chanin

### Assignment 4 Requirements:


1. Import numpy
2. Import pandas
3. Import matplotlib
4. run functions.py
5. Push screenshots to repository

![A4-P1](img/a4p1.png)

![A4-P2](img/a4p2.png)

![A4-P3](img/a4p3.png)

![A4-P4](img/a4p4.png)