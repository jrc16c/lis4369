def get_requirements():
    print("Miles Per Gallon")
    print("\nProgram Requirements:\n"
        + "1. Convert MPG.\n"
        + "2. Must use float data type for user input and calculation.\n"
        + "3. Format and round conversion to two decimal places.")


def calculate_miles_per_gallon():
    #initialize variables
    miles = 0.0
    gal = 0.0
    mpg = 0.0

#get user data
print("\nUser Input:")
miles = float(input("Enter miles driven: " ))
gal = float(input("Enter gallons of fuel used: " ))

#calculate
mpg = round(miles / gal, 2)


#display results
print("\nProgram Output:")
print("{0:,.2f}".format(miles), "miles driven and", "{0:,.2f}".format(gal), "gallons used = " "{0:,.2f}".format(mpg), " mpg")