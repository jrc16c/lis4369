import random


def get_requirements():
    print("Pseudo-Random Number Generator")
    print("\nProgram Requirements:\n"
        + "1. Get user beginning and ending integer values, and store in two variables.\n"
        + "2. Display 10 pseudo-random numbers between, and including, above values.\n"
        + "3. Must use integer data types.\n"
        + "4. Example 1: Using range() and randint() functions."
        + "5. Example 2: Using a list with range() and shuffle functions."
        + "6. Create a program that mirrors the following IPO (input/process/output) format."
                + "Create empty dictionary, using curly braces {}: my_dictionary = {}"
                + "Use the following keys: fname, lname, degree, major, gpa"
        + "Note: Dictionaries have key-value pairs instead of single values; this differentiates a dictionary from a set")

def random_numbers():
    start = 0
    end = 0

    #IPO: Input > Process > Output
    # Get user data
    print("Input:")
    start = int(input("Enter beginning value: "))
    end = int(input("Enter ending value: "))

    #Process and Output
    #Display 10 random numbers
    print("\nOutput:")
    print("Example 1: Using range() and randint() functions: ")
    for count in range(10):
        print(random.randint(start, end), sep=", ", end=" ")
        #sleep(1)

    print()

    print("\nExample 2: Using a list, with range() and shuffle() functions:")
    #
    #
    r = list(range(start, end +1))
    random.shuffle(r)
    for i in r:
        print(i, sep=", ", end=" ")
    
    print()