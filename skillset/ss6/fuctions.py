def get_requirements():
    print("Python Looping Structures")
    print("\nProgram Requirements:\n"
        + "1. Print while loop.\n"
        + "2. Print for loops using range() function, and implicit and explicit lists.\n"
        + "3. Use break and continue statements.\n"
        + "4. Replicate display below.")

def selection_structures():
    #1
    print("\n1. while loop:")
    i = 1
    while i <= 3:
        print(i)
        i = i +1
    #2
    print("\n2. for loop: using range() function with 1 arg")
    for i in range(4):
        print(i)
    #3
    print("\n3. for loop using range() function with two args")
    for i in range(1, 4):
        print(i)
    #4
    print("\n4. for loop: using range() function with three args (interval 2):")
    for i in range(1, 4, 2):
        print(i)
    #5
    print("\n5. for loop: using range() function with three args (negative interval):")
    for i in range(3, 0, -2):
        print(i)
    #6
    print("\n6. for loop using (implicit) list:")
    for i in [1, 2, 3]:
        print(i)
    #7
    print("\n7. for loop iterating through (explicit) string list:")
    states = ['Michigan', 'Alabama', 'Florida']
    for state in states:
        print(state)
    #8
    print("\n8. for loop using break statement (stops loop):")   
    states = ['Michigan', 'Alabama', 'Florida'] 
    for state in states:
        if state == "Florida":
            break
        print(state)
    #9
    print("\n9. for loop using continue statement (stops and continues with next):")
    states = ['Michigan', 'Alabama', 'Florida']
    for state in states:
        if state == "Alabama":
            continue
        print(state)
    #10
    print("\n10. print list length:")
    states = ['Michigan', 'Alabama', 'Florida']
    print(len(states))
    

