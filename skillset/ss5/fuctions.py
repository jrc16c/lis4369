def get_requirements():
    print("Python Selection Structures")
    print("\nProgram Requirements:\n"
        + "1. Use Python selection structure.\n"
        + "2. Prompt user for two numbers, and a suitable operator.\n"
        + "3. Test for correct numeric operator.\n"
        + "4. Replicate display below.")

def selection_structures():
#get user data
    print("\nPython Calculator:")
    num1 = float(input("Enter num1: " ))
    num2 = float(input("Enter num2: " ))

    print("\nSuitable Operators: +, -, *, /, // (integer division), % (modulo operator), **(power), pow(import math) ")
    op = input("Enter operator: ")

    import math

    if op in ['+']:
        add = num1 + num2
        print(add)
    elif op in ['-']:
        sub = num1 - num2
        print(sub)
    elif op in ['*']:
        mult = num1 * num2
        print(mult)
    elif op in ['/']:
        div = (num1 / num2)
        print(div)
    elif op in ['%']:
        mod = num1 % num2
        print(mod)
    elif op in ['//']:
        slash = (num1 // num2)
        print(slash)
    elif op in ['**']:
        exp = num1**num2
        print(exp)
    elif op in ['pow']:
        pow = math.pow(num1, num2)
        print(pow)
    elif op != ['+', '-', '*', '/', '//', '%', '**']:
        print("Incorrect operator")

