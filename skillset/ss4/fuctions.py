def get_requirements():
    print("Calorie Percentage")
    print("\nProgram Requirements:\n"
        + "1. Find calories per grams of fat, carbs, and protein.\n"
        + "2. Calculate percentages.\n"
        + "3. Must use float data type.\n"
        + "4. Format, right-align numbers, and round to two decimal places.")


#get user data
print("\nUser Input:")
fat = float(input("Enter total fat grams: " ))
carb = float(input("Enter total carb grams: " ))
pro = float(input("Enter total protein grams: " ))

#calculate calories
fatcal = fat * 9
carbcal = carb * 4
procal = pro * 4

#calculate percentage
total = fatcal + carbcal + procal
fatper = (fatcal / total)
carbper = (carbcal / total)
proper = (procal / total)


#display results
print("\nOutput:")
print("{0:12} {1:10} {2:10}".format("Type" , "Calories", "Percentage"))
print("{0:10} {1:>10.2f} {2:>10.2%}".format("Fat ", fatcal, fatper))
print("{0:10} {1:>10.2f} {2:>10.2%}".format("Carbs ", carbcal, carbper))
print("{0:10} {1:>10.2f} {2:>10.2%}".format("Protein ", procal, proper))
