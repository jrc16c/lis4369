#!/usr/bin/env python3

#Program requirements:

print("Square Feet to Acres")
print("\nProgram Requirements:\n"
    + "1. Research: number of square feet to acre of land.\n"
    + "2. Must use float data type for user input and calculation.\n"
    + "3. Format and round conversion to two decimal places.")

print("\nUser Input:")
feet = float(input("Enter square feet: " ))

#calculate
acres = round(feet / 43560, 2)


#display results
print("\nProgram Output:")
print("{0:,.2f}".format(feet), "square feet = ", "{0:,.2f}".format(acres), " acres")
