def get_requirements():
    print("IT/ICT Student Percentage")
    print("\nProgram Requirements:\n"
        + "1. Find number of IT/ICT students in class.\n"
        + "2. Calculate IT/ICT Student Percentage.\n"
        + "3. Must use float data type (to facilitate right-alignment).\n"
        + "4. Format and round conversion to two decimal places.")


#get user data
print("\nUser Input:")
it = float(input("Enter number of IT students: " ))
ict = float(input("Enter number of ICT students: " ))

#calculate
total = it + ict
itper = (it / total)
ictper = (ict / total)


#display results
print("\nProgram Output:")
print("{0:17} {1:>5.2f}".format("Total Students", total))
print("{0:17} {1:>5.2%}".format("IT Students", itper))
print("{0:17} {1:>5.2%}".format("ICT Students", ictper))