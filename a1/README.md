# LIS 4369

## Joshua Chanin

### Assignment 1 Requirements:

1. Install Python
2. Install R
3. Install R Studio
4. Install Visual Studio Code
5. Create a1_tip_calculator application
6. Provide screenshots of installations
7. Create Bitbucket repo
8. Complete Bitbucket tutorial (bitbucktstationlocations)
9. Provide git command descriptions

### Git Command Descriptions
1. git init - create a new local repository
2. gir status - List the files you've changed and those you still need to add or commit
3. git add - add file
4. git commit -m - Commit changes to head (but not yet to the remote repository)
5. git push - Send changes to the master branch of your remote repository
6. git pull - Fetch and merge changes on the remote server to your working directory
7. git remote -v - List all currently configured remote repositories

![A1-Code](img/code.png)

![A1-Output](img/output.png)

![A1-idle](img/idle.png)